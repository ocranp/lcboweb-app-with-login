package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if(loginName == "" || loginName.isEmpty()) {
			return false;
		}
		
		Pattern loginRegex = Pattern.compile("^[a-zA-Z0-9]{6,}+$");
		Matcher matcher = loginRegex.matcher(loginName);
		return matcher.find();
	}
}
