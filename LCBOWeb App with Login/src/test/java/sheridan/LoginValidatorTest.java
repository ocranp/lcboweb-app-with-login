package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "portia123" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login - login name should atleast 6 characters/digits" , LoginValidator.isValidLoginName( "portia" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login - login name should be atleast 6 characters/digits" , LoginValidator.isValidLoginName( "porti" ) );
	}

	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login - login name is 0 characters" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersandNumbersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "portia123" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersandNumbersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "por123" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersandNumbersBoundaryOut( ) {
		assertFalse("Invalid login - no special characters allowed" , LoginValidator.isValidLoginName( "portia123!" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersandNumbersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
	}

}
